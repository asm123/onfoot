package com.asm.lab.onfoot.rest.model;

import java.util.HashMap;
import java.util.Map;

public abstract class Request {
    protected String url;
    protected Map<String, String> params;
    protected Map<String, String> headers;

    public Request(String url) {
        this.url = url;
        params = new HashMap<>();
        headers = new HashMap<>();
    }

    public String getUrl() {
        return url;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public void addParam(String key, String value) {
        params.put(key, value);
    }
}
