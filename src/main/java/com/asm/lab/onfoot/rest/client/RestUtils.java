package com.asm.lab.onfoot.rest.client;

import com.asm.lab.onfoot.rest.model.Request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class RestUtils {
    static String getUrlWithParams(Request request) throws UnsupportedEncodingException {
        String paramString = makeParamString(request.getParams());
        return paramString.isEmpty() ? request.getUrl() : request.getUrl() + "?" + paramString;
    }

    private static String makeParamString(Map<String, String> params) throws UnsupportedEncodingException {
        List<String> paramStrings = new ArrayList<>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String paramString = key + "=" + URLEncoder.encode(value, "UTF-8");
            paramStrings.add(paramString);
        }
        String[] paramsArray = paramStrings.toArray(new String[0]);
        return String.join("&", paramsArray);
    }
}
