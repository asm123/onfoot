package com.asm.lab.onfoot.rest.client;

import com.asm.lab.onfoot.rest.model.GetRequest;
import com.asm.lab.onfoot.rest.model.PostRequest;
import com.asm.lab.onfoot.rest.model.Response;

import java.io.IOException;

public interface RestClient {

    Response get(GetRequest request) throws IOException;

    Response post(PostRequest request) throws IOException;
}
