package com.asm.lab.onfoot.rest.client;

import com.asm.lab.onfoot.rest.model.GetRequest;
import com.asm.lab.onfoot.rest.model.PostRequest;
import com.asm.lab.onfoot.rest.model.Response;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.util.Map;

public class DefaultRestClient implements RestClient {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private HttpClient client;

    public DefaultRestClient() {
        client = HttpClients.createDefault();
    }

    @Override
    public Response get(GetRequest request) throws IOException {
        String url = RestUtils.getUrlWithParams(request);
        HttpGet httpGet = new HttpGet(url);
        return fetchResponse(httpGet, request.getHeaders());
    }

    @Override
    public Response post(PostRequest request) throws IOException {
        String url = RestUtils.getUrlWithParams(request);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(request.getBody()));
        return fetchResponse(httpPost, request.getHeaders());
    }

    private Response fetchResponse(HttpUriRequest request, Map<String, String> headers) throws IOException {
        headers.forEach(request::addHeader);
        HttpResponse httpResponse = client.execute(request);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
        StringBuilder body = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            body.append(line);
        }
        return new Response(statusCode, body.toString());
    }

}
