package com.asm.lab.onfoot.rest.model;

import java.util.HashMap;

public class PostRequest extends Request {
    private String body;

    public PostRequest(String url, String body) {
        super(url);
        this.url = url;
        this.body = body;
        params = new HashMap<>();
        headers = new HashMap<>();
    }

    public String getBody() {
        return body;
    }

}
