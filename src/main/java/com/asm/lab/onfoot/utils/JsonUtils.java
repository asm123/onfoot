package com.asm.lab.onfoot.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class JsonUtils {

    public static JsonNode toJson(String jsonString) throws IOException {
        return new ObjectMapper().readTree(jsonString);
    }

    public static JsonNode fromInputStream(InputStream is) throws IOException {
        return new ObjectMapper().readTree(is);
    }
}
