package com.asm.lab.onfoot.planner.entities;

import java.util.List;

public class Trip {
    private List<Venue> venues;
    private double walkingDistance;

    public Trip(List<Venue> venues, double walkingDistance) {
        this.venues = venues;
        this.walkingDistance = walkingDistance;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public double getWalkingDistance() {
        return walkingDistance;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "venues=" + venues +
                ", walkingDistance=" + walkingDistance +
                '}';
    }
}
