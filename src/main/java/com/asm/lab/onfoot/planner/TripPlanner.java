package com.asm.lab.onfoot.planner;

import com.asm.lab.onfoot.exceptions.HttpFailureException;
import com.asm.lab.onfoot.foursquare.FoursquareClient;
import com.asm.lab.onfoot.foursquare.config.FoursquareConfig;
import com.asm.lab.onfoot.foursquare.model.SearchQuery;
import com.asm.lab.onfoot.planner.algorithms.VenuePlanner;
import com.asm.lab.onfoot.planner.entities.Trip;
import com.asm.lab.onfoot.planner.entities.Venue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TripPlanner {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private FoursquareConfig config;

    public TripPlanner(FoursquareConfig config) {
        this.config = config;
    }

    public List<Trip> planTrip(String city, Double startLat, Double startLng, int startTime, String interest,
                               int maxWalking, int maxPlaces)
            throws IOException, HttpFailureException {
        List<Venue> allVenues = getAllVenues(city, startLat, startLng, interest);
        List<Trip> tripSuggestions = VenuePlanner
                .planVenues(allVenues, startLat, startLng, startTime, maxWalking, maxPlaces);
        log.info("Trip suggestions: {}", tripSuggestions.size());
        rankTrips(tripSuggestions);
        return tripSuggestions.stream().limit(10).collect(Collectors.toList());
    }

    private List<Venue> getAllVenues(String city, Double lat, Double lng, String category) throws IOException, HttpFailureException {
        List<String> categoryIds = Collections.singletonList(config.getCategories().getId(category));
        SearchQuery query = new SearchQuery()
                .setNear(city)
                .setCategoryIds(categoryIds)
                .setLimit(20);
        if (lat != null && lng != null) {
            query.setLL(lat, lng);
        }

        FoursquareClient foursquareClient = new FoursquareClient(config.getCredentials());
        List<Venue> allVenues = foursquareClient.searchVenues(query);
        return dedupeVenues(allVenues);
    }

    private List<Venue> dedupeVenues(List<Venue> venues) {
        return venues.stream().distinct().collect(Collectors.toList());
    }

    private void rankTrips(List<Trip> trips) {
        trips.sort((t1, t2) -> {
            int v1 = t1.getVenues().size();
            int v2 = t2.getVenues().size();
            return v1 == v2 ? Double.compare(t1.getWalkingDistance(), t2.getWalkingDistance()) : Integer.compare(v2, v1);
        });
    }
}
