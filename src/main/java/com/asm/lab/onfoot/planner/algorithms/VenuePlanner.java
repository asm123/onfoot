package com.asm.lab.onfoot.planner.algorithms;

import com.asm.lab.onfoot.planner.entities.Trip;
import com.asm.lab.onfoot.planner.entities.Venue;
import com.asm.lab.onfoot.utils.HaversineDistance;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.KShortestSimplePaths;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class VenuePlanner {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static List<Trip> planVenues(List<Venue> allVenues, Double startLat, Double startLng,
                                        int startTime, int maxWalking, int maxPlaces) {
        Venue start = new Venue("start", "start");
        start.setLatitude(startLat);
        start.setLongitude(startLng);

        List<Venue> augmented = new ArrayList<>();
        augmented.add(start);
        augmented.addAll(allVenues);

        return generateTripSuggestions(augmented, start, maxWalking, maxPlaces);
    }

    private static Graph<Venue, DefaultWeightedEdge> buildGraph(List<Venue> venues) {
        Graph<Venue, DefaultWeightedEdge> graph = GraphTypeBuilder.<Venue, DefaultWeightedEdge>undirected()
                .weighted(true)
                .edgeClass(DefaultWeightedEdge.class)
                .buildGraph();
        for (Venue venue : venues) {
            graph.addVertex(venue);
        }
        for (int i = 0; i < venues.size(); i++) {
            Venue v1 = venues.get(i);
            for (int j = i + 1; j < venues.size(); j++) {
                Venue v2 = venues.get(j);
                double distance = HaversineDistance.distance(v1.getLatitude(), v1.getLongitude(),
                        v2.getLatitude(), v2.getLongitude());
                DefaultWeightedEdge edge = graph.addEdge(v1, v2);
                graph.setEdgeWeight(edge, distance);
            }
        }
        return graph;
    }

    private static List<Trip> generateTripSuggestions(List<Venue> allVenues,
                                                      Venue start, int maxWalking, int maxPlaces) {
        Graph<Venue, DefaultWeightedEdge> graph = buildGraph(allVenues);
        List<GraphPath<Venue, DefaultWeightedEdge>> shortestPaths = new ArrayList<>();
        Set<DefaultWeightedEdge> edgesFromStart = new HashSet<>(graph.edgesOf(start))
                .stream().filter(edge -> graph.getEdgeWeight(edge) < maxWalking)
                .collect(Collectors.toSet());
        log.info("Edges from start: {}", edgesFromStart.size());
        for (DefaultWeightedEdge edge : edgesFromStart) {
            Venue v1 = graph.getEdgeSource(edge);
            Venue v2 = graph.getEdgeTarget(edge);
            graph.removeEdge(edge);
            List<GraphPath<Venue, DefaultWeightedEdge>> paths = findPaths(graph, v1, v2, maxPlaces);
            List<GraphPath<Venue, DefaultWeightedEdge>> topPaths = filterPaths(graph, paths, maxWalking, maxPlaces);
            shortestPaths.addAll(topPaths);
            double distance = HaversineDistance.distance(v1.getLatitude(), v1.getLongitude(),
                    v2.getLatitude(), v2.getLongitude());
            DefaultWeightedEdge newEdge = graph.addEdge(v1, v2);
            graph.setEdgeWeight(newEdge, distance);
        }
        log.info("Total trips: {}", shortestPaths.size());
        List<Trip> allTrips = shortestPaths.stream().map(path -> {
            List<Venue> venues = path.getVertexList();
            venues.add(start);
            double pathWeight = getPathWeight(graph, path);
            return new Trip(venues, pathWeight);
        }).collect(Collectors.toList());
        return dedupeTrips(allTrips);
    }

    private static List<GraphPath<Venue, DefaultWeightedEdge>> findPaths(Graph<Venue, DefaultWeightedEdge> graph,
                                                                         Venue v1, Venue v2, int maxHops) {
        KShortestSimplePaths<Venue, DefaultWeightedEdge> algorithm = new KShortestSimplePaths<>(graph, maxHops);
        return algorithm.getPaths(v1, v2, 3);
    }

    private static List<GraphPath<Venue, DefaultWeightedEdge>> filterPaths(Graph<Venue, DefaultWeightedEdge> graph,
                                                                           List<GraphPath<Venue, DefaultWeightedEdge>> paths,
                                                                           int maxWalking, int maxPlaces) {
        List<GraphPath<Venue, DefaultWeightedEdge>> filteredPaths = paths.stream().filter(path -> {
            double pathWeight = getPathWeight(graph, path);
            return pathWeight <= maxWalking && path.getEdgeList().size() <= maxPlaces;
        }).collect(Collectors.toList());
        return pickTopPaths(filteredPaths);
    }

    private static List<GraphPath<Venue, DefaultWeightedEdge>> pickTopPaths(List<GraphPath<Venue, DefaultWeightedEdge>> allPaths) {
        int maxLength = allPaths.stream().mapToInt(path -> path.getEdgeList().size()).max().orElse(0);
        return allPaths.stream().filter(path -> path.getEdgeList().size() == maxLength).collect(Collectors.toList());
    }

    private static double getPathWeight(Graph<Venue, DefaultWeightedEdge> graph, GraphPath<Venue, DefaultWeightedEdge> path) {
        double pathWeight = 0.0;
        Venue start = path.getStartVertex();
        Venue end = path.getEndVertex();
        pathWeight += HaversineDistance.distance(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude());
        List<DefaultWeightedEdge> pathEdges = path.getEdgeList();
        for (DefaultWeightedEdge edge : pathEdges) {
            pathWeight += graph.getEdgeWeight(edge);
        }
        return pathWeight;
    }

    private static List<Trip> dedupeTrips(List<Trip> trips) {
        List<Trip> uniqueTrips = new ArrayList<>();
        boolean[] processed = new boolean[trips.size()];
        for (int i = 0; i < trips.size(); i++) {
            if (processed[i]) {
                continue;
            }
            processed[i] = true;
            Trip t1 = trips.get(i);
            uniqueTrips.add(t1);
            for (int j = i + 1; j < trips.size(); j++) {
                Trip t2 = trips.get(j);
                if (haveSameVenues(t1, t2)) {
                    processed[j] = true;
                    Trip t = t1.getWalkingDistance() < t2.getWalkingDistance() ? t1 : t2;
                    uniqueTrips.set(uniqueTrips.size() - 1, t);
                }
            }
        }
        return uniqueTrips;
    }

    private static boolean haveSameVenues(Trip t1, Trip t2) {
        if (t1.getVenues().size() != t2.getVenues().size()) {
            return false;
        }
        Set<String> t1Names = t1.getVenues().stream().map(Venue::getName).collect(Collectors.toSet());
        Set<String> t2Names = t2.getVenues().stream().map(Venue::getName).collect(Collectors.toSet());
        Set<String> intersection = new HashSet<>(t1Names);
        intersection.retainAll(t2Names);
        return intersection.size() == t1Names.size();
    }

}
