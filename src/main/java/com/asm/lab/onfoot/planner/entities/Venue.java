package com.asm.lab.onfoot.planner.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Venue {
    private String id;
    private String name;
    private String description;

    private List<String> categories;
    private Double latitude;
    private Double longitude;
    private String address;
    private String city;
    private String country;

    private Integer likes;
    private Double rating;

    public Venue(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Venue setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getLikes() {
        return likes;
    }

    public Venue setLikes(Integer likes) {
        this.likes = likes;
        return this;
    }

    public Double getRating() {
        return rating;
    }

    public Venue setRating(Double rating) {
        this.rating = rating;
        return this;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getCategories() {
        return categories;
    }

    public Venue setCategories(List<String> categories) {
        this.categories = categories;
        return this;
    }

    public void addCategory(String category) {
        if (categories == null) {
            categories = new ArrayList<>();
        }
        categories.add(category);
    }

    public Double getLatitude() {
        return latitude;
    }

    public Venue setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Venue setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Venue setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Venue setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Venue setCountry(String country) {
        this.country = country;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venue venue = (Venue) o;
        return id.equals(venue.id) &&
                Objects.equals(latitude, venue.latitude) &&
                Objects.equals(longitude, venue.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, latitude, longitude);
    }

    @Override
    public String toString() {
        return "Venue{" +
                "name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
