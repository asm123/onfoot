package com.asm.lab.onfoot.foursquare.model;

import com.asm.lab.onfoot.foursquare.converters.VenueConverter;
import com.asm.lab.onfoot.planner.entities.Venue;
import com.fasterxml.jackson.databind.JsonNode;

public class GetVenueResponse {
    private Venue venue;

    public GetVenueResponse(JsonNode responseBody) {
        JsonNode venueJson = responseBody.get("response").get("venue");
        this.venue = VenueConverter.jsonToVenue(venueJson);
    }

    public Venue getVenue() {
        return venue;
    }

}
