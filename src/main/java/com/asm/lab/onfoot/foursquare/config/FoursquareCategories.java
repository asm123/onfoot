package com.asm.lab.onfoot.foursquare.config;

import com.asm.lab.onfoot.utils.JsonUtils;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FoursquareCategories {
    private Map<String, String> idToName;
    private Map<String, String> nameToId;

    private static final String CATEGORIES_PATH = "/foursquare/categories.json";

    public FoursquareCategories() throws IOException {
        idToName = new HashMap<>();
        nameToId = new HashMap<>();

        JsonNode fullJson = JsonUtils.fromInputStream(getClass().getResourceAsStream(CATEGORIES_PATH));
        JsonNode categoriesJson = fullJson.get("response").get("categories");
        for (JsonNode category : categoriesJson) {
            String name = category.get("name").asText();
            String id = category.get("id").asText();
            idToName.put(id, name);
            nameToId.put(name, id);
        }
    }

    public String getId(String category) {
        return nameToId.get(category);
    }

    public String getName(String id) {
        return idToName.get(id);
    }
}
