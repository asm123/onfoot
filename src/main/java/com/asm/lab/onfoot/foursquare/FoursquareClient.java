package com.asm.lab.onfoot.foursquare;

import com.asm.lab.onfoot.exceptions.HttpFailureException;
import com.asm.lab.onfoot.foursquare.config.FoursquareCredentials;
import com.asm.lab.onfoot.foursquare.model.GetVenueResponse;
import com.asm.lab.onfoot.foursquare.model.SearchQuery;
import com.asm.lab.onfoot.foursquare.model.SearchVenuesResponse;
import com.asm.lab.onfoot.planner.entities.Venue;
import com.asm.lab.onfoot.rest.client.DefaultRestClient;
import com.asm.lab.onfoot.rest.client.RestClient;
import com.asm.lab.onfoot.rest.model.GetRequest;
import com.asm.lab.onfoot.rest.model.Request;
import com.asm.lab.onfoot.rest.model.Response;
import com.asm.lab.onfoot.utils.JsonUtils;

import java.io.IOException;
import java.util.List;

import static com.asm.lab.onfoot.foursquare.config.FoursquareCredentials.*;

public class FoursquareClient {
    private static final String SEARCH_VENUES_API = "https://api.foursquare.com/v2/venues/search";
    private static final String GET_VENUE_API = "https://api.foursquare.com/v2/venues/%s";


    private String clientId;
    private String clientSecret;
    private String version;

    public FoursquareClient(String clientId, String clientSecret, String version) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.version = version;
    }

    public FoursquareClient(FoursquareCredentials credentials) {
        this.clientId = credentials.getProperty(CLIENT_ID);
        this.clientSecret = credentials.getProperty(CLIENT_SECRET);
        this.version = credentials.getProperty(VERSION);
    }

    public List<Venue> searchVenues(SearchQuery query) throws IOException, HttpFailureException {
        assert query.hasLL() || query.hasNear() : "At least one of \"ll\" and \"near\" must be provided";

        GetRequest request = new GetRequest(SEARCH_VENUES_API);
        addAuth(request);

        if (query.hasLL()) {
            request.addParam("ll", query.getLatitude() + "," + query.getLongitude());
        }
        if (query.hasNear()) {
            request.addParam("near", query.getNear());
        }
        if (query.hasRadius()) {
            request.addParam("radius", String.valueOf(query.getRadius()));
        }
        if (query.hasQuery()) {
            request.addParam("query", query.getQuery());
        }
        if (query.hasLimit()) {
            request.addParam("limit", String.valueOf(query.getLimit()));
        }
        if (query.hasCategoryIds()) {
            String[] categories = query.getCategoryIds().toArray(new String[0]);
            request.addParam("categoryId", String.join(",", categories));
        }
        RestClient client = new DefaultRestClient();
        Response response = client.get(request);
        if (response.getStatusCode() != 200) {
            throw new HttpFailureException(response);
        }
        SearchVenuesResponse searchResponse = new SearchVenuesResponse(JsonUtils.toJson(response.getBody()));
        return searchResponse.getVenues();
    }

    public Venue getVenue(String venueId) throws IOException, HttpFailureException {
        String getVenuesAPI = String.format(GET_VENUE_API, venueId);
        GetRequest request = new GetRequest(getVenuesAPI);
        addAuth(request);
        Response response = new DefaultRestClient().get(request);
        if (response.getStatusCode() != 200) {
            throw new HttpFailureException(response);
        }
        GetVenueResponse getVenueResponse = new GetVenueResponse(JsonUtils.toJson(response.getBody()));
        return getVenueResponse.getVenue();
    }

    private void addAuth(Request request) {
        request.addParam("client_secret", clientSecret);
        request.addParam("client_id", clientId);
        request.addParam("v", version);
    }

}
