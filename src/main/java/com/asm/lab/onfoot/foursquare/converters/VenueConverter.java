package com.asm.lab.onfoot.foursquare.converters;

import com.asm.lab.onfoot.planner.entities.Venue;
import com.fasterxml.jackson.databind.JsonNode;

public class VenueConverter {

    public static Venue jsonToVenue(JsonNode venueJson) {
        String id = venueJson.get("id").asText();
        String name = venueJson.get("name").asText();
        Venue venue = new Venue(id, name);

        if (venueJson.has("location")) {
            JsonNode location = venueJson.get("location");
            venue.setLatitude(location.get("lat").asDouble());
            venue.setLongitude(location.get("lng").asDouble());
            if (location.has("address")) {
                venue.setAddress(location.get("address").asText());
            }
            if (location.has("city")) {
                venue.setCity(location.get("city").asText());
            }
            if (location.has("country")) {
                venue.setCountry(location.get("country").asText());
            }
        }

        if (venueJson.has("categories")) {
            JsonNode categories = venueJson.get("categories");
            for (JsonNode category : categories) {
                venue.addCategory(category.get("name").asText());
            }
        }

        if (venueJson.has("rating")) {
            venue.setRating(venueJson.get("rating").asDouble());
        }
        if (venueJson.has("likes")) {
            venue.setLikes(venueJson.get("likes").get("count").asInt());
        }

        return venue;
    }

}
