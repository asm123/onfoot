package com.asm.lab.onfoot.foursquare.model;

import com.asm.lab.onfoot.foursquare.converters.VenueConverter;
import com.asm.lab.onfoot.planner.entities.Venue;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class SearchVenuesResponse {

    private List<Venue> venues;

    public SearchVenuesResponse(JsonNode responseBody) {
        venues = new ArrayList<>();
        JsonNode venuesArray = responseBody.get("response").get("venues");
        for (JsonNode venueJson : venuesArray) {
            Venue venue = VenueConverter.jsonToVenue(venueJson);
            venues.add(venue);
        }
    }

    public List<Venue> getVenues() {
        return venues;
    }

}
