package com.asm.lab.onfoot.foursquare.config;

import java.io.IOException;

public class FoursquareConfig {
    private FoursquareCredentials credentials;
    private FoursquareCategories categories;

    public FoursquareConfig() throws IOException {
        credentials = new FoursquareCredentials();
        categories = new FoursquareCategories();
    }

    public FoursquareCredentials getCredentials() {
        return credentials;
    }

    public FoursquareCategories getCategories() {
        return categories;
    }
}
