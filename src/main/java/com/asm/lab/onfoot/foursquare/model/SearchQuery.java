package com.asm.lab.onfoot.foursquare.model;

import java.util.Collections;
import java.util.List;

public class SearchQuery {
    private Double latitude;
    private Double longitude;
    private String near;
    private Integer radius;
    private String query;
    private Integer limit;
    private List<String> categoryIds;

    public Double getLatitude() {
        return latitude;
    }

    public SearchQuery setLL(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getNear() {
        return near;
    }

    public SearchQuery setNear(String near) {
        this.near = near;
        return this;
    }

    public Integer getRadius() {
        return radius;
    }

    public SearchQuery setRadius(Integer radius) {
        this.radius = radius;
        return this;
    }

    public String getQuery() {
        return query;
    }

    public SearchQuery setQuery(String query) {
        this.query = query;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public SearchQuery setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public SearchQuery setCategoryIds(List<String> categoryIds) {
        this.categoryIds = Collections.unmodifiableList(categoryIds);
        return this;
    }

    public boolean hasLL() {
        return latitude != null && longitude != null;
    }

    public boolean hasNear() {
        return near != null;
    }

    public boolean hasRadius() {
        return radius != null;
    }

    public boolean hasQuery() {
        return query != null;
    }

    public boolean hasLimit() {
        return limit != null;
    }

    public boolean hasCategoryIds() {
        return categoryIds != null && !categoryIds.isEmpty();
    }
}
