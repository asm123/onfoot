package com.asm.lab.onfoot.foursquare.config;

import com.asm.lab.onfoot.utils.JsonUtils;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class FoursquareCredentials {
    private static final String CONFIG_PATH = "/foursquare/credentials.json";

    public static final String CLIENT_SECRET = "client.secret";
    public static final String CLIENT_ID = "client.id";
    public static final String VERSION = "version";

    private JsonNode config;

    public FoursquareCredentials() throws IOException {
        config = JsonUtils.fromInputStream(getClass().getResourceAsStream(CONFIG_PATH));
    }

    public String getProperty(String name) {
        return config.get(name).asText();
    }

}
