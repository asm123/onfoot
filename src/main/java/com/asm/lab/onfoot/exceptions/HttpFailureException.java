package com.asm.lab.onfoot.exceptions;

import com.asm.lab.onfoot.rest.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class HttpFailureException extends Exception {

    public HttpFailureException(Response response) throws JsonProcessingException {
        super(constructMessage(response));
    }

    private static String constructMessage(final Response response) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode root = mapper.createObjectNode();
        root.put("statusCode", response.getStatusCode());
        root.put("message", response.getBody());
        return mapper.writeValueAsString(root);
    }


}
