package com.asm.lab.onfoot.planner;

import com.asm.lab.onfoot.foursquare.config.FoursquareConfig;
import com.asm.lab.onfoot.planner.entities.Trip;
import com.asm.lab.onfoot.exceptions.HttpFailureException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

public class TripPlannerTest {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Test
    public void planTrip() throws IOException, HttpFailureException {
        FoursquareConfig config = new FoursquareConfig();
        TripPlanner planner = new TripPlanner(config);
        String city = "London, UK";
        double startLat = 51.4982191;
        double startLon = -0.1733862;
        int startTime = 482348;
        String interest = "Arts & Entertainment";
        int maxWalking = 10;
        int maxPlaces = 5;

        List<Trip> trips = planner.planTrip(city, startLat, startLon, startTime, interest, maxWalking, maxPlaces);
        Assert.assertFalse(trips.isEmpty());
        trips.forEach(trip -> log.info(trip.toString()));
    }
}