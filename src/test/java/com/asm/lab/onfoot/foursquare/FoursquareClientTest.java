package com.asm.lab.onfoot.foursquare;

import com.asm.lab.onfoot.foursquare.config.FoursquareCredentials;
import com.asm.lab.onfoot.planner.entities.Venue;
import com.asm.lab.onfoot.foursquare.model.SearchQuery;
import com.asm.lab.onfoot.exceptions.HttpFailureException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

public class FoursquareClientTest {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Test
    public void searchVenues() {
        try {
            FoursquareClient client = new FoursquareClient(new FoursquareCredentials());
            SearchQuery query = new SearchQuery()
                    .setQuery("museum")
                    .setNear("London, UK")
                    .setLimit(10);
            List<Venue> venues = client.searchVenues(query);
            Assert.assertFalse(venues.isEmpty());
            venues.forEach(venue -> log.info("{} -> {} -> {} -> {}",
                    venue.getId(), venue.getName(), venue.getLatitude(), venue.getLongitude()));
        } catch (Exception e) {
            Assert.fail("Failed with exception " + e.getMessage());
        }
    }

    @Test
    public void getVenue() {
        try {
            FoursquareClient client = new FoursquareClient(new FoursquareCredentials());
            String venueId = "4daeaa490437710b8137b098";
            Venue venue = client.getVenue(venueId);
            log.info("{} -> {} -> {} -> {}", venue.getId(), venue.getName(), venue.getLatitude(), venue.getLongitude());
        } catch (IOException | HttpFailureException e) {
            Assert.fail("Failed with exception " + e.getMessage());
        }
    }
}